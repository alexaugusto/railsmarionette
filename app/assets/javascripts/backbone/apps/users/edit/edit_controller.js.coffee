@Demo.module "UsersApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->

	Edit.Controller =

		edit: (id, user) ->
			user or= App.request "user:entity", id
			editView = @getEditView user
			editView.on "form:submit", (child, args) =>
				@saveUser child.model, editView
			editView.on "form:cancel", (child, args) =>
				App.vent.trigger "user:cancel"

			App.mainRegion.show editView

		getEditView: (user) ->
			new Edit.User
				model: user

		saveUser: (model, editView) ->
			data = Backbone.Syphon.serialize editView
			model.save data
