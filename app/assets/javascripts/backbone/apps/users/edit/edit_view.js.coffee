@Demo.module "UsersApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->

	class Edit.User extends Marionette.ItemView
		template: "users/edit/templates/edit_user"

		tagName: "form"
		attributes: ->
			"data-type": @getFormDataType()

		modelEvents:
			"sync" : "render"

		triggers:
			"submit" : "form:submit"
			"click [data-form-button='cancel']"	: "form:cancel"

		getFormDataType: ->
			if @model.isNew() then "new" else "edit"