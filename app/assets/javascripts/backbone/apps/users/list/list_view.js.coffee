@Demo.module "UsersApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Layout extends Marionette.Layout
		template: "users/list/templates/list_layout"
		
		regions:
			panelRegion: "#panel-region"
			usersRegion: "#users-region"
	
	class List.Panel extends Marionette.ItemView
		template: "users/list/templates/_panel"
		triggers:
			"click #newUser" : "user:new:clicked"
	
	class List.User extends Marionette.ItemView
		template: "users/list/templates/_user"
		tagName: "tr"
		triggers:
			"click button.btn-xs" : "user:delete:clicked"
			"click td.full-name" : "user:edit:clicked"
	
	class List.Empty extends Marionette.ItemView
		template: "users/list/templates/_empty"
		tagName: "tr"
	
	class List.Users extends Marionette.CompositeView
		template: "users/list/templates/_users"
		itemView: List.User
		emptyView: List.Empty
		itemViewContainer: "tbody"