@Demo.module "UsersApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	List.Controller =
		
		listUsers: ->
			App.request "user:entities", (users) =>
			
				@layout = @getLayoutView()
			
				@layout.on "show", =>
					@showPanel users
					@showUsers users
			
				App.mainRegion.show @layout
		
		showPanel: (users) ->
			panelView = @getPanelView users

			panelView.on "user:new:clicked", =>
				App.vent.trigger "user:new"

			@layout.panelRegion.show panelView
		
		showUsers: (users) ->
			usersView = @getUsersView users

			usersView.on "itemview:user:edit:clicked", (child, args) =>
				App.vent.trigger "user:edit:clicked", args.model

			usersView.on "itemview:user:delete:clicked", (child, args) =>
				model = args.model
				if confirm "Tem certeza que deseja deletar #{model.get("full_name")}?" then model.destroy() else false

			@layout.usersRegion.show usersView
		
		getUsersView: (users) ->
			new List.Users
				collection: users
			
		getPanelView: (users) ->
			new List.Panel
				collection: users
		
		getLayoutView: ->
			new List.Layout
