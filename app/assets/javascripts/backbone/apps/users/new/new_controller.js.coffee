@Demo.module "UsersApp.New", (New, App, Backbone, Marionette, $, _) ->

	New.Controller =

		new:->
			user = App.request "new:user:entity"
			newView = @getNewView user
			newView.on "form:submit", (child, args) =>
				@saveUser child.model, newView
			newView.on "form:cancel", (child, args) =>
				App.vent.trigger "user:cancel"

			App.mainRegion.show newView

		getNewView: (user) ->
			new New.User
				model: user

		saveUser: (model, newView) ->
			data = Backbone.Syphon.serialize newView
			console.log data
			model.save data
			App.vent.trigger "user:cancel"
