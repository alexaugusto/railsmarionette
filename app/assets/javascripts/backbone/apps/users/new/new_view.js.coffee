@Demo.module "UsersApp.New", (New, App, Backbone, Marionette, $, _) ->

	class New.User extends Marionette.ItemView
		template: "users/new/templates/new_user"

		tagName: "form"
		attributes: ->
			"data-type": @getFormDataType()

		triggers:
			"submit" : "form:submit"
			"click [data-form-button='cancel']"	: "form:cancel"

		getFormDataType: ->
			if @model.isNew() then "new" else "edit"