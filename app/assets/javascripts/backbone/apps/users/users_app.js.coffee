@Demo.module "UsersApp", (UsersApp, App, Backbone, Marionette, $, _) ->
	
	class UsersApp.Router extends Marionette.AppRouter
		appRoutes:
			"users/:id/edit" : "edit"
			"users"	: "listUsers"
			"users/new" : "new"
	
	API =
		listUsers: ->
			UsersApp.List.Controller.listUsers()

		edit: (id, user) ->
			UsersApp.Edit.Controller.edit id, user

		new: ->
			UsersApp.New.Controller.new()

	App.vent.on "user:edit:clicked", (user) ->
		Backbone.history.navigate Routes.edit_user_path(user.id)
		API.edit user.id, user

	App.vent.on "user:cancel", ->
		Backbone.history.navigate Routes.users_path()
		API.listUsers()

	App.vent.on "user:new", ->
		Backbone.history.navigate Routes.new_user_path()
		API.new()

	App.addInitializer ->
		new UsersApp.Router
			controller: API