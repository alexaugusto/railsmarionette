@Demo.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
	
	class Entities.Header extends Entities.Model
	
	class Entities.HeaderCollection extends Entities.Collection
		model: Entities.Header
	
	API =
		getHeaders: ->
			new Entities.HeaderCollection [
				{ name: "Users", link: "##{Routes.users_path()}" }
				{ name: "Leads", link: "#" }
				{ name: "Appointments", link: "#" }
			]
	
	App.reqres.setHandler "header:entities", ->
		API.getHeaders()