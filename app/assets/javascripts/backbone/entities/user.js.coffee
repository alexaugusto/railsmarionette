@Demo.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
	
	class Entities.User extends Entities.Model
		urlRoot: -> Routes.users_path()
	
	class Entities.UsersCollection extends Entities.Collection
		model: Entities.User
		url: -> Routes.users_path()
	
	API =
		getUserEntities: (cb) ->
			users = new Entities.UsersCollection
			users.fetch
				success: ->
					cb users

		getUser: (id) ->
			user = new Entities.User
				id: id
			user.fetch()
			user

		newUser: ->
			new Entities.User
	
	App.reqres.setHandler "user:entities", (cb) ->
		API.getUserEntities cb

	App.reqres.setHandler "user:entity", (id) ->
		API.getUser id

	App.reqres.setHandler "new:user:entity", ->
		API.newUser()