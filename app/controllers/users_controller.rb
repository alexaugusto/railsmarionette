class UsersController < ApplicationController
	respond_to :json
	
	def index
		@users = User.all
	end

	def show
		@user = User.find params[:id]
	end

	def create
		@user = User.new
		if @user.update_attributes user_params
			render "users/show"
		else
			respond_with @user
		end
	end

	def update
		@user = User.find params[:id]
		if @user.update_attributes user_params
			render "users/show"
		else
			respond_with @user
		end
	end

	def destroy
		user = User.find params[:id]
		user.destroy()
		render json: {}
	end

	private

	def user_params
		params.require(:user).permit(:first_name, :last_name)
	end

end